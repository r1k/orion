ARCH=i386
CC=i686-elf-gcc

BUILDDIR=build
ISODIR=isodir
ORION_SYSROOT?=sysroot
DESTDIR?=$(ORION_SYSROOT)
BOOTDIR=/boot
INCLUDEDIR=/usr/local/include

ASMFILES=$(wildcard arch/$(ARCH)/*.asm)
ASMOBJS=$(patsubst %.asm, $(BUILDDIR)/%.o, $(ASMFILES))

CFILES=$(wildcard *.c)
COBJS=$(patsubst %.c, $(BUILDDIR)/%.o, $(CFILES))

LIBKFILES=$(wildcard libk/*.c)
LIBKOBJS=$(patsubst %.c, $(BUILDDIR)/%.o, $(LIBKFILES))

INCLUDEFLAGS?=
INCLUDEFLAGS:=-Iinclude -Ilibk/include
DEBUGFLAGS?=
CFLAGS=-std=c99 -ffreestanding -fstack-protector-all $(INCLUDEFLAGS) $(DEBUGFLAGS) -Wall -Wextra

CRTI_OBJ = $(BUILDDIR)/arch/$(ARCH)/crti.o
CRTBEGIN_OBJ:=$(shell $(CC) $(CFLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ:=$(shell $(CC) $(CFLAGS) -print-file-name=crtend.o)
CRTN_OBJ = $(BUILDDIR)/arch/$(ARCH)/crtn.o

OBJFILES = \
$(CRTI_OBJ) \
$(CRTBEGIN_OBJ) \
$(BUILDDIR)/arch/$(ARCH)/boot.o \
$(LIBKOBJS) \
$(COBJS) \
$(CRTEND_OBJ) \
$(CRTN_OBJ)

DIRS = $(BUILDDIR)/arch/$(ARCH) $(BUILDDIR)/libk
KERNELFILE =orion-$(ARCH).bin
ISOFILE =orion-$(ARCH).iso

.PHONY: all clean iso install install-headers run run-grub
.SUFFIXES: .o .c .asm

all: $(DIRS) $(ASMOBJS) $(COBJS) $(LIBKOBJS) $(BUILDDIR)/$(KERNELFILE)

$(DIRS):
	mkdir -p $(BUILDDIR)
	mkdir -p $(BUILDDIR)/arch/$(ARCH)
	mkdir -p $(BUILDDIR)/libk

$(BUILDDIR)/$(KERNELFILE): $(OBJFILES)
	$(CC) -T linker.ld -o $@ $(CFLAGS) $(OBJFILES) -nostdlib -lgcc

$(BUILDDIR)/%.o: %.asm
	nasm -f elf32 $< -o $@

$(BUILDDIR)/%.o: %.c 
	$(CC) -c $< -o $@ $(CFLAGS) -lgcc

$(BUILDDIR)/%.o: libk/%.c
	$(CC) -c $< -o $@ $(CFLAGS) -lgcc

install: all install-headers install-kernel
 
install-headers:
	mkdir -p $(DESTDIR)$(INCLUDEDIR)
	cp -R --preserve=timestamps include/. $(DESTDIR)$(INCLUDEDIR)/.
	cp -R --preserve=timestamps libk/include/. $(DESTDIR)$(INCLUDEDIR)/.
 
install-kernel: $(BUILDDIR)/$(KERNELFILE)
	mkdir -p $(DESTDIR)$(BOOTDIR)
	cp $(BUILDDIR)/$(KERNELFILE) $(DESTDIR)$(BOOTDIR)

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(ISODIR)
	rm -rf $(DESTDIR)

run: install
	qemu-system-$(ARCH) -kernel $(BUILDDIR)/$(KERNELFILE) -serial stdio -S -s

$(BUILDDIR)/$(ISOFILE): install
	mkdir -p $(ISODIR)
	mkdir -p $(ISODIR)/boot
	mkdir -p $(ISODIR)/boot/grub

	cp $(DESTDIR)$(BOOTDIR)/$(KERNELFILE) $(ISODIR)/boot/$(KERNELFILE)

	echo 'menuentry "orion" { ' > $(ISODIR)/boot/grub/grub.cfg
	echo '	multiboot /boot/$(KERNELFILE)' >> $(ISODIR)/boot/grub/grub.cfg
	echo '}' >> $(ISODIR)/boot/grub/grub.cfg

	grub-mkrescue -o $(BUILDDIR)/$(ISOFILE) $(ISODIR)

iso: $(BUILDDIR)/$(ISOFILE)

run-grub: iso
	qemu-system-$(ARCH) -cdrom $(BUILDDIR)/$(ISOFILE) -serial stdio -S -s
	
