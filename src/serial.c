#include <serial.h>
#include <asm.h>
#include <stdio.h>
#include <vga.h>

int serial_init() {
  outb(PORT + 1, 0x00);    // Disable all interrupts
  outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
  outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
  outb(PORT + 1, 0x00);    //                  (hi byte)
  outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
  outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
  outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
  outb(PORT + 4, 0x1E);    // Set in loopback mode, test the serial chip
  outb(PORT + 0, 0xAE);    // Test serial chip (send byte 0xAE and check if serial returns same byte)
  
  // Check if serial is faulty (i.e: not same byte as sent)
  if(inb(PORT + 0) != 0xAE) {
    return 1;
  }
  
  // If serial is not faulty set it in normal operation mode
  // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
  outb(PORT + 4, 0x0F);
  return 0;
}

int is_transmit_empty() {
   return inb(PORT + 5) & 0x20;
}

int dbg_putchar(int c) {
   while (is_transmit_empty() == 0);
   outb(PORT, c);
   return (unsigned char)c;
}

int vdbgf(const char *restrict format, va_list ap) {
  char s[1024] = ""; // FIXME
  int size = kvsprintf(s, format, ap);
  int i = 0;
  while (i < size) {
    dbg_putchar(s[i]);
    i++;
  }
  return size;
}

int dbgf(const char *restrict format, ...) {
  int size;
  va_list ap;
  va_start(ap, format);
  size = vdbgf(format, ap);
  va_end(ap);
  return size;
}

int warn(const char *restrict format, ...) {
  int size;
  va_list ap;
  va_start(ap, format);
  dbgf("\033[1;33mWARNING:\033[0m ");
  size = vdbgf(format, ap);
  va_end(ap);
  return size;
}

int error(const char *restrict format, ...) {
  int size;
  va_list ap;
  va_start(ap, format);
  dbgf("\033[1;31mERROR:\033[0m ");
  size = vdbgf(format, ap);
  va_end(ap);
  return size;
}

int fixme(const char *restrict format, ...) {
  int size;
  va_list ap;
  va_start(ap, format);
  dbgf("\033[32mFIXME:\033[0m ");
  size = vdbgf(format, ap);
  va_end(ap);
  return size;
}

int todo(const char *restrict format, ...) {
  int size;
  va_list ap;
  va_start(ap, format);
  dbgf("\033[92mTODO:\033[0m ");
  size = vdbgf(format, ap);
  va_end(ap);
  return size;
}

// FIXME: maybe this function shouldn't be here
void panic(const char *restrict format, ...) {
  va_list ap;
  va_start(ap, format);
  terminal_setcolor(VGA_COLOR_RED);
  kprintf("PANIC: ");
  terminal_setcolor(VGA_COLOR_LIGHT_GREY);
  dbgf("\033[31mPANIC:\033[0m ");
  kvprintf(format, ap);
  vdbgf(format, ap);
  va_end(ap); // FIXME: not return from this function
}
