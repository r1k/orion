#ifndef IDT_H
#define IDT_H

#include <stdint.h>

#define IRQ0 32
#define IRQ1 33
#define IRQ2 34
#define IRQ3 35
#define IRQ4 36
#define IRQ5 37
#define IRQ6 38
#define IRQ7 39
#define IRQ8 40
#define IRQ9 41
#define IRQ10 42
#define IRQ11 43
#define IRQ12 44
#define IRQ13 45
#define IRQ14 46
#define IRQ15 47

typedef struct {
  uint16_t    isr_low;      // The lower 16 bits of the ISR's address
  uint16_t    kernel_cs;    // The GDT segment selector that the CPU will load into CS before calling the ISR
  uint8_t     reserved;     // Set to zero
  uint8_t     attributes;   // Type and attributes; see the IDT page
  uint16_t    isr_high;     // The higher 16 bits of the ISR's address
} __attribute__((packed)) idt_entry_t;

__attribute__((aligned(0x10))) 

typedef struct {
  uint16_t	limit;
  uint32_t	base;
} __attribute__((packed)) idtr_t;

typedef struct registers {
   uint32_t ds;                  // Data segment selector
   uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; // Pushed by pusha.
   uint32_t int_no, err_code;    // Interrupt number and error code (if applicable)
   uint32_t eip, cs, eflags, useresp, ss; // Pushed by the processor automatically.
} registers_t;

typedef void (*isr_t)(registers_t);

void register_interrupt_handler(uint8_t n, isr_t handler);
void idt_init();

#endif
