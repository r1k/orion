#ifndef SERIAL_H
#define SERIAL_H

#include <stdarg.h>

#define PORT 0x3f8 // COM1

int serial_init();
int dbg_putchar(int c);
int vdbgf(const char *restrict format, va_list ap);
int dbgf(const char *restrict format, ...);
int warn(const char *restrict format, ...);
int error(const char *restrict format, ...);
int fixme(const char *restrict format, ...);
int todo(const char *restrict format, ...);
void panic(const char *restrict format, ...);

#endif
